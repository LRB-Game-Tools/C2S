﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObjectCreator
{
    public partial class Form2 : Form
    {
        public bool subOption_;
        /// <summary>
        /// Declare a new Form2 instance.
        /// </summary>
        /// <param name="subOption">Is the pie menu option you are adding a suboption?</param>
        public Form2(bool subOption)
        {
            InitializeComponent();
            subOption_ = subOption;
        }

        /// <summary>
        /// Declare a new Form2 instance.
        /// </summary>
        public Form2()
        {
            InitializeComponent();
            subOption_ = false;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Program.addOption(listBox1.SelectedItem.ToString(), subOption_);
            }
            catch
            {
                MessageBox.Show("No item was selected.");
            }
            this.Close();
        }
    }
}
