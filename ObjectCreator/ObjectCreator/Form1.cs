﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Windows.Forms;

using TSO.Files;

namespace ObjectCreator
{
    public partial class Form1 : Form
    {
        int rotation = 0; // can be 0, 90, 180, 270.

        // Rotation images for 0, 90, 180, 270
        public static Image rot0;
        public static Image rot90;
        public static Image rot180;
        public static Image rot270;

        // Item for the listbox drag+drop
        public object lb_item = null;

        // Object type
        public string objType = "custom";

        public Form1()
        {
            InitializeComponent();
        }

        private void imageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Open image and put it in the PictureBox; store it also in one of the rotations.  This can be specified depending on the rotation currently selected.
            if (rotation == 0)
            {
                try
                {
                    openFileDialog1.Filter = "PNG Image|*.png|Bitmap Image|*.bmp|All files|*.*";
                    openFileDialog1.FileName = "";
                    openFileDialog1.Title = "Open image";
                    openFileDialog1.ShowDialog();
                    rot0 = Image.FromFile(openFileDialog1.FileName);
                    updateImage();
                }
                catch { }
            }
            else if (rotation == 90)
            {
                try
                {
                    openFileDialog1.Filter = "PNG Image|*.png|Bitmap Image|*.bmp|All files|*.*";
                    openFileDialog1.FileName = "";
                    openFileDialog1.Title = "Open image";
                    openFileDialog1.ShowDialog();
                    rot90 = Image.FromFile(openFileDialog1.FileName);
                    updateImage();
                }
                catch { }
            }
            else if (rotation == 180)
            {
                try
                {
                    openFileDialog1.Filter = "PNG Image|*.png|Bitmap Image|*.bmp|All files|*.*";
                    openFileDialog1.FileName = "";
                    openFileDialog1.Title = "Open image";
                    openFileDialog1.ShowDialog();
                    rot180 = Image.FromFile(openFileDialog1.FileName);
                    updateImage();
                }
                catch { }
            }
            else if (rotation == 270)
            {
                try
                {
                    openFileDialog1.Filter = "PNG Image|*.png|Bitmap Image|*.bmp|All files|*.*";
                    openFileDialog1.FileName = "";
                    openFileDialog1.Title = "Open image";
                    openFileDialog1.ShowDialog();
                    rot270 = Image.FromFile(openFileDialog1.FileName);
                    updateImage();
                }
                catch { }
            }
        }

        public void updateImage()
        {
            if (rotation == 0)
                picBoxObject.BackgroundImage = rot0;
            if (rotation == 90)
                picBoxObject.BackgroundImage = rot90;
            if (rotation == 180)
                picBoxObject.BackgroundImage = rot180;
            if (rotation == 270)
                picBoxObject.BackgroundImage = rot270;

        }

        private void objectToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void behaviorsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // If the object type is a seat, duplicate the default chair and change the sprites.
            if (objType == "seat")
            {
                // Use the template; there isn't one at the moment, but I'll create one!

            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.subOption_ = false;
            form2.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (rotation != 0)
            {
                rotation -= 90;
                lblRotation.Text = "Current rotation: " + rotation.ToString();
            }
            else
            {
                rotation = 270;
                lblRotation.Text = "Current rotation: " + rotation.ToString();
            }
            updateImage();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (rotation != 270)
            {
                rotation += 90;
                lblRotation.Text = "Current rotation: " + rotation.ToString();
            }
            else
            {
                rotation = 0;
                lblRotation.Text = "Current rotation: " + rotation.ToString();
            }
            updateImage();
        }

        private void btnAddSubOption_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.subOption_ = true;
            form2.Show();
        }



        private void lboxOptions_DragLeave(object sender, EventArgs e)
        {
            ListBox lb = sender as ListBox;

            lb_item = lb.SelectedItem;
            lb.Items.Remove(lb.SelectedItem);
        }

        private void lboxOptions_DragEnter(object sender, DragEventArgs e)
        {
            if (lb_item != null)
            {
                lboxOptions.Items.Add(lb_item);
                lb_item = null;
            }
        }


        private void lboxOptions_MouseDown(object sender, MouseEventArgs e)
        {
            lb_item = null;

            if (lboxOptions.Items.Count == 0)
            {
                return;
            }

            int index = lboxOptions.IndexFromPoint(e.X, e.Y);
            string s = lboxOptions.Items[index].ToString();
            DragDropEffects dde1 = DoDragDrop(s, DragDropEffects.All);
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            lb_item = null;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                int item = lboxOptions.SelectedIndex;
                lboxOptions.Items.Insert(item - 1, lboxOptions.SelectedItem.ToString());
                lboxOptions.SetSelected(item - 1, true);
                lboxOptions.Items.RemoveAt(item + 1);
            }
            catch { }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                int item = lboxOptions.SelectedIndex;
                lboxOptions.Items.Insert(item + 1, lboxOptions.SelectedItem.ToString());
                lboxOptions.SetSelected(item + 1, true);
                lboxOptions.Items.RemoveAt(item - 1);
            }
            catch { }
        }

        private void customToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Feature not yet implemented!", "C2S for Project Dollhouse", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
        }

        private void seatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create a new seating object; duplicate the default chair.  This is just a sprite change.
            objType = "seat";
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TSO.Files.formats.iff.IffChunk testObj = new TSO.Files.formats.iff.chunks.SPR2();
            testObj.Read(new TSO.Files.formats.iff.Iff("./template.iff"), new System.IO.FileStream("./template.iff", System.IO.FileMode.Open));
            MemoryStream ms = new MemoryStream(testObj.ChunkData);
            picBoxObject.Image = System.Drawing.Image.FromStream(ms);
            MessageBox.Show("done");
        }
    }
}
