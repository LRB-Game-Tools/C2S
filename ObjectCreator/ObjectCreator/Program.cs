﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObjectCreator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static Form1 form1 = new Form1();
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(form1);
        }

        static public void addOption(string strItem, bool subOption)
        {
            if (subOption)
            {
                form1.lboxOptions.Items.Add("\t-" + strItem);
            }
            else
            {
                form1.lboxOptions.Items.Add(strItem);
            }
        }
    }
}
